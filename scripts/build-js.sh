#!/usr/bin/bash

set -e

echo ">> Building Libraries..."
uglifyjs node_modules/jquery/dist/jquery.js --compress --screw-ie8 --define --stats --keep-fnames -o build/lib/jquery.js
uglifyjs node_modules/bootstrap/dist/js/bootstrap.js --compress --screw-ie8 --define --stats --keep-fnames -o build/lib/bootstrap.js

echo ">> Building Application JS..."
browserify -t reactify src/js/app.js -o build/js/app.js
uglifyjs build/js/app.js --compress --screw-ie8 --define --stats --keep-fnames -o build/js/app.js

echo ">> Building Main JS..."
uglifyjs src/main.js --compress --screw-ie8 --define --stats --keep-fnames -o build/main.js

echo ">> Building Audio Controller..."
uglifyjs src/js/audio-controller.js --compress --screw-ie8 --define --stats --keep-fnames -o build/js/audio-controller.js
uglifyjs src/js/audio-player.js --compress --screw-ie8 --define --stats --keep-fnames -o build/js/audio-player.js
