var BrowserWindow = require('browser-window'); 
var ipc = require('ipc');

var playerWindow;

function startWindow() {
	playerWindow = new BrowserWindow({
		width: 0,
		height: 0,
		show: false,
		frame: false,
		transparent: true,
		'skip-taskbar': true
	});
	playerWindow.loadUrl('file://' + __dirname + '/audio.html');
	buildEvents();
	return playerWindow;
}

function buildEvents() {
	playerWindow.on('closed', function () { delete playerWindow; });
}

function getWindow() {
	return playerWindow;
}

function play() {
	console.log("CLICK!");
	playerWindow.webContents.executeJavascript("audioPlayer.play()");
}

function setType(type) {
	playerWindow.webContents.executeJavascript("changeType(" + type + ");");
}

ipc.on('play', function () {
	play();
});

module.exports = {
	getWindow: getWindow,
	startWindow: startWindow,
	play: play
};