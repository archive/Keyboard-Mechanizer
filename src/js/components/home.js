var React = require('react');

var NavSidebar = require('./nav-sidebar');

var Home = React.createClass({
  render: function () {
    return (
      <div className="container-fluid row">
        <div className="col-xs-3">
          <NavSidebar />
        </div>
        <div className="col-xs-9">
          <h2>Title</h2>
        </div>
      </div>
    );
  },
});

module.exports = Home;