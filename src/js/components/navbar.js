var React = require('react');
var ipc = window.require('ipc');

var NavBar = React.createClass({
  handleClose: function () {
    ipc.send('close');
  },

  handleHide: function () {
    ipc.send('hide');
  },

  render: function () {
    return (
      <nav id="navbar" className="navbar navbar-default navbar-fixed-top">
        <div className="container-fluid">
          <a className="navbar-brand"><img src="img/icon@300.png" />Keyboard Mechanizer</a>
          <ul className="nav navbar-nav navbar-right">
            <li><a href="#" onClick={this.handleHide}>Hide</a></li>
            <li><a href="#" onClick={this.handleClose} title="Close Program"><i className="icon ion-close-round"></i></a></li>
          </ul>
        </div>
      </nav>
    );
  }
});

module.exports = NavBar;