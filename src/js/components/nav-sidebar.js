var React = require('react');

var NavSidebar = React.createClass({
  render: function () {
    return (
      <ul className="nav nav-pills nav-stacked">
        <li role="presentation" className="active"><a href="#">Settings</a></li>
        <li role="presentation"><a href="#">Test</a></li>
        <li role="presentation"><a href="#">About</a></li>
      </ul>
    );
  }
});

module.exports = NavSidebar;