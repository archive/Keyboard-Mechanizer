var React = require('react');
var Home = require('./components/home');
var NavBar = require('./components/navbar');

window.$ = window.jQuery = window.require('./lib/jquery.js');

React.render(<NavBar />, document.getElementById('NavBarContainer'));
React.render(<Home />, document.getElementById('AppContainer'));