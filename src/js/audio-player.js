var audioPlayer = new Audio();
var audioPath, audioType;

function changeType(type) {
	audioType = type;
	audioPath = 'audio/' + type + '.mp3';
	audioPlayer.src = audioPath;
}