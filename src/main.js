var app = require('app'); 
var BrowserWindow = require('browser-window'); 
var Menu = require('menu');
var globalShortcut = require('global-shortcut')
var fs = require('fs');
var Tray = require('tray');
var ipc = require('ipc');

var AudioController = require('./js/audio-controller');

require('crash-reporter').start();  // Start crash reporter

// Creating menu
var menu = new Menu();
Menu.setApplicationMenu(menu);
var mainWindow = null;
var icon = null;
const INDEX_PAGE = 'file://' + __dirname + '/index.html';

// Make sure the application closes
app.on('window-all-closed', function () {
	app.quit();
});

app.on('ready', function () {
	icon = new Tray(__dirname + '/img/icon.png');
	icon.setToolTip('Keyboard Mechanizer | Click to show');
	icon.on('clicked', function () {
		console.log("Icon Clicked");
		mainWindow.show();
	});

	mainWindow = new BrowserWindow({
		width: 800,
		height: 600,
		frame: false,
		resizable: false,
		title: "Keyboard Mechanizer",
		icon: "img/icon@300.png"
	});
	mainWindow.loadUrl(INDEX_PAGE);
	mainWindow.toggleDevTools();
	mainWindow.on('closed', function() {
		delete mainWindow;
		AudioController.getWindow().close();
	});

	AudioController.startWindow();
});

ipc.on('hide', function () {
	mainWindow.hide();
	mainWindow.loadUrl('');
});

ipc.on('show', function () {
	mainWindow.show();
	mainWindow.loadUrl(INDEX_PAGE);
});

ipc.on('close', function () {
	mainWindow.close();
});